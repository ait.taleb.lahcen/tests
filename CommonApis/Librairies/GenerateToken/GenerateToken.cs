﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;

namespace CommonApi.Librairies.GenerateToken
{
    public class GenerateToken
    {
            public static string GetToken(string authServerBaseUri,string realm, string authClientId ,string authClientSecret)
        {
            string url = $"{authServerBaseUri}/realms/{realm}/protocol/openid-connect/token";
            Console.WriteLine("URL " +url);
            var client = new RestClient(url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"grant_type=client_credentials&client_id={authClientId}&client_secret={authClientSecret}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            dynamic data = JObject.Parse(response.Content);
            return (string)data.access_token;

        }

        public static string GetTokenByUserCredicals()
        {
              string url = "https://ssoco-dc1-fr-test.platform.vpgrp.io/auth/realms/vpgrp/protocol/openid-connect/token";
              string client_id = "admin-cli";
              string username = "";
              string password = "";
              string grant_type = "password";

              var client = new RestClient(url);
              var request = new RestRequest(Method.POST);
              request.AddHeader("content-type", "application/x-www-form-urlencoded");
              request.AddParameter("application/x-www-form-urlencoded", $"client_id={client_id}&username={username}&password={password}&grant_type={grant_type}", ParameterType.RequestBody);
              IRestResponse response = client.Execute(request);
              dynamic data = JObject.Parse(response.Content);          
              return (string)data.access_token;

        }


    }
}
